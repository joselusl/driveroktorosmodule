//////////////////////////////////////////////////////
//  parrotARDroneOuts.h
//
//  Created on: Jul 3, 2013
//      Author: joselusl
//
//  Last modification on: Dec 23, 2013
//      Author: joselusl
//
//  Last modification on: Jun 9, 2014
//      Author: Alexander Cerón
//
//  Description: file to manage the oktor sensor data
//
//
//////////////////////////////////////////////////////


#ifndef _OKTO_OUTS_H
#define _OKTO_OUTS_H


#include "droneModuleROS.h"
#include "communication_definition.h"
#include "sensor_msgs/Imu.h"

#include "droneMsgsROS/battery.h"
#include "droneMsgsROS/vector2Stamped.h"

#include "okto_driver/OktoSensorData.h"


//RotationAngles
#include "geometry_msgs/Vector3Stamped.h"

//// ROS  ///////
#include "ros/ros.h"



//I/O stream
//std::cout
#include <iostream>

//Math
//M_PI
#include <cmath>


/////////////////////////////////////////
// Class Battery
//
//   Description: Needs to be adjusted the unit!
//
/////////////////////////////////////////
class BatteryROSModule : public DroneModule
{

    //Publisher
protected:
    ros::Publisher BatteryPubl;
    bool publishBatteryValue(const droneMsgsROS::battery &BatteryMsgs);


    //Subscriber
protected:
    ros::Subscriber BatterySubs;
    void batteryCallback(const okto_driver::OktoSensorData &msg);


    //Battery msgs
protected:
//    droneMsgsROS::battery BatteryMsgs;


    //Constructors and destructors
public:
    BatteryROSModule();
    ~BatteryROSModule();


    //Init and close
public:
    void init();
    void close();

    //Open
 public:
    void open(ros::NodeHandle & nIn, std::string moduleName);

    //Reset
protected:
    bool resetValues();

    //Start
protected:
    bool startVal();

    //Stop
protected:
    bool stopVal();

    //Run
public:
    bool run();


};


/////////////////////////////////////////
// Class RotationAngles
//
//   Description
//
/////////////////////////////////////////
class RotationAnglesROSModule : public DroneModule
{

    //Publisher
protected:
    ros::Publisher RotationAnglesPubl;
    bool publishRotationAnglesValue(const geometry_msgs::Vector3Stamped &RotationAnglesMsgs);


    //Subscriber
protected:
    ros::Subscriber RotationAnglesSubs;
   void rotationAnglesCallback(const okto_driver::OktoSensorData &msg);

    //RotationAngles msgs
protected:
//    geometry_msgs::Vector3Stamped RotationAnglesMsgs;


    //Constructors and destructors
public:
    RotationAnglesROSModule();
    ~RotationAnglesROSModule();


    //Init and close
public:
    void init();
    void close();

    //Open
 public:
    void open(ros::NodeHandle & nIn, std::string moduleName);

    //Reset
protected:
    bool resetValues();

    //Start
protected:
    bool startVal();

    //Stop
protected:
    bool stopVal();

    //Run
public:
    bool run();


};

#endif
