//Drone
#include "oktoOuts.h"


using namespace std;


////// Battery ////////
BatteryROSModule::BatteryROSModule() : DroneModule(droneModule::active)
{
    init();
    return;
}

BatteryROSModule::~BatteryROSModule()
{

    return;
}

void BatteryROSModule::init()
{

}

void BatteryROSModule::close()
{

}

void BatteryROSModule::open(ros::NodeHandle & nIn, std::string moduleName)
{
    //Node
    DroneModule::open(nIn,moduleName);

    //init();


    //Configuration


    //Publisher
    BatteryPubl = n.advertise<droneMsgsROS::battery>(DRONE_DRIVER_SENSOR_BATTERY, 1, true);


    //Subscriber
    BatterySubs=n.subscribe("okto_sensor_data", 1, &BatteryROSModule::batteryCallback, this);


    //Flag of module opened
    droneModuleOpened=true;

    //Auto-Start module
    moduleStarted=true;

    //End
    return;
}

//Reset
bool BatteryROSModule::resetValues()
{
    return true;
}

//Start
bool BatteryROSModule::startVal()
{
    return true;
}

//Stop
bool BatteryROSModule::stopVal()
{
    return true;
}

//Run
bool BatteryROSModule::run()
{
    if(!DroneModule::run())
    {
        return false;
    }

    return true;
}


void BatteryROSModule::batteryCallback(const okto_driver::OktoSensorData &msg)
{
    //Asynchronous module with only one callback!
    if(!run())
        return;

    droneMsgsROS::battery BatteryMsgs;

    // Read Battery from navdata
    BatteryMsgs.header.stamp   = ros::Time::now();
    // minimum voltage per cell used by the "digital battery checker" is 3.55 V
    // msg.voltage is in V
    BatteryMsgs.batteryPercent = ( msg.voltage - 14.2)/(16.8-14.2) * 100;

    publishBatteryValue(BatteryMsgs);
    return;
}


bool BatteryROSModule::publishBatteryValue(const droneMsgsROS::battery &BatteryMsgs)
{
    if(droneModuleOpened==false)
        return false;

    BatteryPubl.publish(BatteryMsgs);
    return true;
}


////// RotationAngles ////////
RotationAnglesROSModule::RotationAnglesROSModule() : DroneModule(droneModule::active)
{
    init();
    return;
}

RotationAnglesROSModule::~RotationAnglesROSModule()
{

    return;
}



void RotationAnglesROSModule::init()
{

}

void RotationAnglesROSModule::close()
{

}

void RotationAnglesROSModule::open(ros::NodeHandle & nIn, std::string moduleName)
{
    //Node
    DroneModule::open(nIn,moduleName);

    //init();


    //Configuration


    //Publisher
    RotationAnglesPubl = n.advertise<geometry_msgs::Vector3Stamped>(DRONE_DRIVER_SENSOR_ROTATION_ANGLES, 1, true);


    //Subscriber
    RotationAnglesSubs=n.subscribe("okto_sensor_data", 1, &RotationAnglesROSModule::rotationAnglesCallback, this);

    //Flag of module opened
    droneModuleOpened=true;

    //Auto-Start module
    moduleStarted=true;

    //End
    return;
}

//Reset
bool RotationAnglesROSModule::resetValues()
{
    return true;
}

//Start
bool RotationAnglesROSModule::startVal()
{
    return true;
}

//Stop
bool RotationAnglesROSModule::stopVal()
{
    return true;
}

//Run
bool RotationAnglesROSModule::run()
{
    if(!DroneModule::run())
    {
        return false;
    }

    return true;
}

void RotationAnglesROSModule::rotationAnglesCallback(const okto_driver::OktoSensorData &msg)
{
    //Asynchronous module with only one callback!
    if(!run())
        return;

    // deg,   mavwork reference frame
    geometry_msgs::Vector3Stamped RotationAnglesMsgs;
    RotationAnglesMsgs.header.stamp =  ros::Time::now();
    RotationAnglesMsgs.vector.x =  (-1.0)*((double)msg.ang_roll)/10.0;
    RotationAnglesMsgs.vector.y =  (-1.0)*((double)msg.ang_nick)/10.0;
    RotationAnglesMsgs.vector.z =  (+1.0)*((double)msg.gyro_compass_heading);

    publishRotationAnglesValue(RotationAnglesMsgs);
    return;
}

bool RotationAnglesROSModule::publishRotationAnglesValue(const geometry_msgs::Vector3Stamped &RotationAnglesMsgs)
{
    if(droneModuleOpened==false)
        return false;

    RotationAnglesPubl.publish(RotationAnglesMsgs);
    return true;
}
